<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns="http://maven.apache.org/POM/4.0.0"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <artifactId>treasure-map-api</artifactId>
  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <artifactId>maven-release-plugin</artifactId>
          <configuration>
            <arguments>-Dmaven.javadoc.skip=true -Dmaven.test.skipTests=true -Dmaven.test.skip=true
            </arguments>
            <tagNameFormat>v@{project.version}</tagNameFormat>
          </configuration>
          <groupId>org.apache.maven.plugins</groupId>
          <version>2.5.3</version>
        </plugin>
        <plugin>
          <artifactId>maven-compiler-plugin</artifactId>
          <configuration>
            <annotationProcessorPaths>
              <path>
                <artifactId>lombok</artifactId>
                <groupId>org.projectlombok</groupId>
                <version>${lombok.version}</version>
              </path>
              <path>
                <artifactId>mapstruct-processor</artifactId>
                <groupId>org.mapstruct</groupId>
                <version>${mapstruct.version}</version>
              </path>
            </annotationProcessorPaths>
            <source>${java.version}</source>
            <target>${java.version}</target>
          </configuration>
          <groupId>org.apache.maven.plugins</groupId>
          <version>${maven-compiler-plugin.version}</version>
        </plugin>

        <!-- Plugins for code analysing by sonarqube -->
        <plugin>
          <artifactId>sonar-maven-plugin</artifactId>
          <groupId>org.sonarsource.scanner.maven</groupId>
          <version>3.9.1.2184</version>
        </plugin>
        <plugin>
          <artifactId>jacoco-maven-plugin</artifactId>
          <groupId>org.jacoco</groupId>
          <version>0.8.7</version>
        </plugin>
      </plugins>
    </pluginManagement>
    <plugins>
      <plugin>
        <artifactId>spring-boot-maven-plugin</artifactId>
        <configuration>
          <excludes>
            <exclude>
              <artifactId>lombok</artifactId>
              <groupId>org.projectlombok</groupId>
            </exclude>
          </excludes>
        </configuration>
        <groupId>org.springframework.boot</groupId>
      </plugin>
      <plugin>
        <artifactId>liquibase-maven-plugin</artifactId>
        <configuration>
          <propertyFile>src/main/resources/application.yml</propertyFile>
          <propertyFileWillOverride>true</propertyFileWillOverride>
        </configuration>
        <groupId>org.liquibase</groupId>
      </plugin>
    </plugins>
  </build>
  <dependencies>
    <dependency>
      <artifactId>spring-boot-starter-data-jpa</artifactId>
      <groupId>org.springframework.boot</groupId>
    </dependency>
    <dependency>
      <artifactId>spring-boot-starter-web</artifactId>
      <groupId>org.springframework.boot</groupId>
    </dependency>
    <dependency>
      <artifactId>spring-boot-devtools</artifactId>
      <groupId>org.springframework.boot</groupId>
      <optional>true</optional>
      <scope>runtime</scope>
    </dependency>

    <!-- Manage validator -->
    <dependency>
      <artifactId>hibernate-validator</artifactId>
      <groupId>org.hibernate.validator</groupId>
      <version>8.0.0.Final</version>
    </dependency>

    <!-- Manage database -->
    <dependency>
      <artifactId>liquibase-core</artifactId>
      <groupId>org.liquibase</groupId>
    </dependency>
    <dependency>
      <artifactId>h2</artifactId>
      <groupId>com.h2database</groupId>
      <scope>runtime</scope>
    </dependency>

    <!-- Manage annotation and mapping-->
    <dependency>
      <artifactId>lombok</artifactId>
      <groupId>org.projectlombok</groupId>
      <optional>true</optional>
    </dependency>
    <dependency>
      <artifactId>javax.annotation-api</artifactId>
      <groupId>javax.annotation</groupId>
      <version>1.3.2</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/javax.validation/validation-api -->
    <dependency>
      <artifactId>validation-api</artifactId>
      <groupId>javax.validation</groupId>
      <version>2.0.1.Final</version>
    </dependency>
    <dependency>
      <artifactId>mapstruct</artifactId>
      <groupId>org.mapstruct</groupId>
      <version>${mapstruct.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.mapstruct/mapstruct-processor -->
    <dependency>
      <artifactId>mapstruct-processor</artifactId>
      <groupId>org.mapstruct</groupId>
      <scope>provided</scope>
      <version>${mapstruct.version}</version>
    </dependency>

    <!-- Manage tests-->
    <dependency>
      <artifactId>spring-boot-starter-test</artifactId>
      <groupId>org.springframework.boot</groupId>
      <scope>test</scope>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api -->
    <dependency>
      <artifactId>junit-jupiter-api</artifactId>
      <groupId>org.junit.jupiter</groupId>
      <scope>test</scope>
      <version>5.9.1</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.instancio/instancio-junit -->
    <dependency>
      <artifactId>instancio-junit</artifactId>
      <groupId>org.instancio</groupId>
      <version>2.6.0</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.mockito/mockito-core -->
    <dependency>
      <artifactId>mockito-core</artifactId>
      <groupId>org.mockito</groupId>
      <scope>test</scope>
      <version>4.9.0</version>
    </dependency>

    <!-- Manage documentation-->
    <dependency>
      <artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
      <groupId>org.springdoc</groupId>
      <version>2.0.4</version>
    </dependency>
  </dependencies>

  <description>quest of treasure</description>
  <groupId>org.quest</groupId>
  <modelVersion>4.0.0</modelVersion>
  <name>treasure-map-api</name>
  <parent>
    <artifactId>spring-boot-starter-parent</artifactId>
    <groupId>org.springframework.boot</groupId>
    <relativePath/>
    <version>3.0.5</version> <!-- lookup parent from repository -->
  </parent>
  <properties>
    <java.version>17</java.version>
    <lombok.version>1.18.24</lombok.version>
    <mapstruct.version>1.5.3.Final</mapstruct.version>
  </properties>

  <version>0.0.1-SNAPSHOT</version>

</project>
