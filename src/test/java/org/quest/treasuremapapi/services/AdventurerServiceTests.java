package org.quest.treasuremapapi.services;

import static org.instancio.Select.field;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.instancio.Instancio;
import org.instancio.junit.InstancioExtension;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.quest.treasuremapapi.exceptions.NullArgumentException;
import org.quest.treasuremapapi.generictypes.Box;
import org.quest.treasuremapapi.generictypes.services.AdventurerService;
import org.quest.treasuremapapi.model.Adventurer;
import org.quest.treasuremapapi.model.Plain;
import org.quest.treasuremapapi.model.types.OrientationTypes;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith({SpringExtension.class, InstancioExtension.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AdventurerServiceTests {

  private static final String ERROR_MESSAGE = "Adventurer provided is null.";
  private AdventurerService adventurerService;

  @BeforeAll
  void init() {

    adventurerService = new AdventurerServiceImpl();
  }

  @Test
  void givenAnAdventurerSouthWhenStepForwardThenCallMethode() {
    //GIVEN
    List<Box> plains1 = new ArrayList<>();
    List<Box> plains2 = new ArrayList<>();
    List<Box> plains3 = new ArrayList<>();
    List<Box> plains4 = new ArrayList<>();
    for (int i = 0; i < 3; i++) {
      plains1.add(Plain.getInstance());
    }
    for (int i = 0; i < 3; i++) {
      plains2.add(Plain.getInstance());
    }
    for (int i = 0; i < 3; i++) {
      plains3.add(Plain.getInstance());
    }
    for (int i = 0; i < 3; i++) {
      plains4.add(Plain.getInstance());
    }

    var adventurer = Instancio
        .of(Adventurer.class)
        .set(field(Adventurer::getOrientation), OrientationTypes.SOUTH)
        .set(field(Adventurer::getVertical), 1)
        .set(field(Adventurer::getHorizontal), 1)
        .create();

    List<List<Box>> boxesLists = new ArrayList<>();
    boxesLists.add(plains1);
    boxesLists.add(plains2);
    boxesLists.add(plains3);
    boxesLists.add(plains4);
    boxesLists
        .get(1)
        .set(1, adventurer);

    //WHEN
    adventurerService.stepFroward(adventurer, boxesLists, List.of(adventurer));

    //THEN
    assertEquals(2, adventurer.getVertical());
  }

  @Test
  void givenAnAdventurerNorthWhenStepForwardThenCallMethode() {
    //GIVEN
    var adventurer = Instancio
        .of(Adventurer.class)
        .set(field(Adventurer::getOrientation), OrientationTypes.NORTH)
        .set(field(Adventurer::getVertical), 2)
        .create();

    //WHEN
    adventurerService.stepFroward(adventurer, null, null);

    //THEN
    assertEquals(1, adventurer.getVertical());
  }

  @Test
  void givenAnAdventurerWestWhenStepForwardThenCallMethode() {
    //GIVEN
    var adventurer = Instancio
        .of(Adventurer.class)
        .set(field(Adventurer::getOrientation), OrientationTypes.WEST)
        .set(field(Adventurer::getHorizontal), 2)
        .create();

    //WHEN
    adventurerService.stepFroward(adventurer, null, null);

    //THEN
    assertEquals(1, adventurer.getHorizontal());
  }

  @Test
  void givenAnAdventurerEastWhenStepForwardThenCallMethode() {
    //GIVEN
    var adventurer = Instancio
        .of(Adventurer.class)
        .set(field(Adventurer::getOrientation), OrientationTypes.EAST)
        .set(field(Adventurer::getHorizontal), 2)
        .create();

    //WHEN
    adventurerService.stepFroward(adventurer, null, null);

    //THEN
    assertEquals(3, adventurer.getHorizontal());
  }

  @Test
  void givenAnAdventurerSouthWhenTurnToRightThenCallMethode() {
    //GIVEN
    var adventurer = Instancio
        .of(Adventurer.class)
        .set(field(Adventurer::getOrientation), OrientationTypes.SOUTH)
        .create();

    //WHEN
    adventurerService.turnToTheRight(adventurer);

    //THEN
    assertEquals(OrientationTypes.WEST, adventurer.getOrientation());
  }

  @Test
  void givenAnAdventurerNorthWhenTurnToRightThenCallMethode() {
    //GIVEN
    var adventurer = Instancio
        .of(Adventurer.class)
        .set(field(Adventurer::getOrientation), OrientationTypes.NORTH)
        .create();

    //WHEN
    adventurerService.turnToTheRight(adventurer);

    //THEN
    assertEquals(OrientationTypes.EAST, adventurer.getOrientation());
  }

  @Test
  void givenAnAdventurerEastWhenTurnToRightThenCallMethode() {
    //GIVEN
    var adventurer = Instancio
        .of(Adventurer.class)
        .set(field(Adventurer::getOrientation), OrientationTypes.EAST)
        .create();

    //WHEN
    adventurerService.turnToTheRight(adventurer);

    //THEN
    assertEquals(OrientationTypes.SOUTH, adventurer.getOrientation());
  }

  @Test
  void givenAnAdventurerWestWhenTurnToRightThenCallMethode() {
    //GIVEN
    var adventurer = Instancio
        .of(Adventurer.class)
        .set(field(Adventurer::getOrientation), OrientationTypes.WEST)
        .create();

    //WHEN
    adventurerService.turnToTheRight(adventurer);

    //THEN
    assertEquals(OrientationTypes.NORTH, adventurer.getOrientation());
  }

  @Test
  void givenAnAdventurerNorthWhenTurnToLeftThenCallMethode() {
    //GIVEN
    var adventurer = Instancio
        .of(Adventurer.class)
        .set(field(Adventurer::getOrientation), OrientationTypes.NORTH)
        .create();

    //WHEN
    adventurerService.turnToTheLeft(adventurer);

    //THEN
    assertEquals(OrientationTypes.WEST, adventurer.getOrientation());
  }

  @Test
  void givenAnAdventurerSouthWhenTurnToLeftThenCallMethode() {
    //GIVEN
    var adventurer = Instancio
        .of(Adventurer.class)
        .set(field(Adventurer::getOrientation), OrientationTypes.SOUTH)
        .create();

    //WHEN
    adventurerService.turnToTheLeft(adventurer);

    //THEN
    assertEquals(OrientationTypes.EAST, adventurer.getOrientation());
  }

  @Test
  void givenAnAdventurerEastWhenTurnToLeftThenCallMethode() {
    //GIVEN
    var adventurer = Instancio
        .of(Adventurer.class)
        .set(field(Adventurer::getOrientation), OrientationTypes.EAST)
        .create();

    //WHEN
    adventurerService.turnToTheLeft(adventurer);

    //THEN
    assertEquals(OrientationTypes.NORTH, adventurer.getOrientation());
  }

  @Test
  void givenNullAdventurerWhenCallMoveMethodsThenThrow() {
    //GIVEN
    List<List<Box>> boxesLists = Collections.emptyList();
    //WHEN

    //THEN
    assertThrows(NullArgumentException.class,
        () -> adventurerService.turnToTheLeft(null), ERROR_MESSAGE);
    assertThrows(NullArgumentException.class,
        () -> adventurerService.turnToTheRight(null), ERROR_MESSAGE);
    assertThrows(NullArgumentException.class,
        () -> adventurerService.stepFroward(null, boxesLists, null), ERROR_MESSAGE);
  }
}
