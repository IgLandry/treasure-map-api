package org.quest.treasuremapapi.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import org.instancio.Instancio;
import org.instancio.junit.InstancioExtension;
import org.instancio.junit.InstancioSource;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.quest.treasuremapapi.services.utils.FileServices;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith({SpringExtension.class, InstancioExtension.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FilesServicesTests {

  private static final String INPUT_FILE_NAME = "src/test/resources/files/input/treasures-input.txt";
  private static final String OUTPUT_PATH = "src/test/resources/files/output";
  private static final String OUTPUT_FILE_NAME = "treasures-output";
  private static final String NOT_EXIST_FILE_NAME = "//////not-exist.txt";
  private FileServices fileServices;

  @BeforeAll
  void init() {

    fileServices = new FileServices();
  }

  @Test
  void givenFileNameWhenCallReadFileMethodThenReturnTheContains() {
    //GIVEN

    //WHEN
    final var strings = fileServices.readFile(INPUT_FILE_NAME);

    //THEN
    assertEquals(10, strings.size());
  }

  @Test
  void givenNotExistingFileNameWhenCallReadFileMethodThenReturnEmptyList() {
    //GIVEN

    //WHEN
    final var emptyStrings = fileServices.readFile(NOT_EXIST_FILE_NAME);

    //THEN
    assertTrue(emptyStrings.isEmpty());
  }

  @ParameterizedTest
  @InstancioSource
  void givenNotExistingFileNameWhenCallReadFileMethodThenThrowException(String firstString,
                                                                        String secondString) {
    //GIVEN
    final var stringList = List.of(firstString, secondString);

    //WHEN
    final var writeFile = fileServices.writeFile(stringList,
        String.join("/", OUTPUT_PATH, OUTPUT_FILE_NAME));

    //THEN
    final var readStrings = fileServices.readFile(writeFile);
    assertEquals(stringList.size(), readStrings.size());
  }

  @Test
  @InstancioSource
  void givenNotExistingPathWhenCallWriteFileMethodThenReturnNull() {
    //GIVEN
    final var stringList = Instancio
        .ofList(String.class)
        .size(2)
        .create();

    //WHEN
    final var writeFile = fileServices.writeFile(stringList, NOT_EXIST_FILE_NAME);

    //THEN
    assertNull(writeFile);
  }
}