package org.quest.treasuremapapi.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.instancio.junit.InstancioExtension;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.quest.treasuremapapi.generictypes.services.HuntManager;
import org.quest.treasuremapapi.services.utils.BoxServices;
import org.quest.treasuremapapi.services.utils.FileServices;
import org.quest.treasuremapapi.services.utils.MapServices;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith({SpringExtension.class, InstancioExtension.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class HuntMangerTests {

  private HuntManager huntManager;

  @BeforeAll
  void init() {

    huntManager = new HuntManagerImpl(new FileServices(), new MapServices(), new BoxServices(),
        new AdventurerManagerImpl(new AdventurerServiceImpl()));
  }

  @Test
  void testManageHunt() {

    final var boxes = huntManager.manageHunt();
    assertFalse(boxes.isEmpty());
    assertEquals(2, boxes.size());
    assertFalse(boxes
        .get(0)
        .isEmpty());
    assertFalse(boxes
        .get(1)
        .isEmpty());
  }
}
