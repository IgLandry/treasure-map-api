package org.quest.treasuremapapi.resources;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.instancio.junit.InstancioExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.quest.treasuremapapi.TreasureMapApiApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith({SpringExtension.class, InstancioExtension.class})
@SpringBootTest(classes = TreasureMapApiApplication.class)
@AutoConfigureMockMvc
class HuntResourcesIT {

  @Autowired
  private MockMvc restMockMvc;

  @Test
  void shouldReturnBadRequestWhenSourceTypeNotProvided() throws Exception {

    restMockMvc
        .perform(
            get("/api/treasure/v1/hunt"))
        .andExpect(status().isOk());
  }
}
