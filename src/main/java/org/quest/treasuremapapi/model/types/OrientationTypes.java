package org.quest.treasuremapapi.model.types;

public enum OrientationTypes {

  NORTH("N"),
  SOUTH("S"),
  EAST("E"),
  WEST("O");
  private final String orientationValue;

  OrientationTypes(String value) {

    this.orientationValue = value;
  }

  /**
   * @return the value (letter) corresponding to each orientation types.
   */
  public String getOrientationValue() {

    return this.orientationValue;
  }
}
