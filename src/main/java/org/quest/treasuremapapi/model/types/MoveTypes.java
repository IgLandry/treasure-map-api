package org.quest.treasuremapapi.model.types;

public enum MoveTypes {
  MOVE("A"),
  LEFT("G"),
  RIGHT("D");
  private final String moveValue;

  MoveTypes(String value) {

    this.moveValue = value;
  }

  /**
   * @return the value (letter) of our box type enum.
   */
  public String getMoveValue() {

    return this.moveValue;
  }
}
