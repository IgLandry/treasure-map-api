package org.quest.treasuremapapi.model.types;

public enum BoxTypes {
  CARD("C"),
  MOUNTAIN("M"),
  TREASURE("T"),
  ADVENTURER("A"),
  PLAIN("P");
  private final String boxValue;

  BoxTypes(String value) {

    this.boxValue = value;
  }

  /**
   * @return the value (letter) of our box type enum.
   */
  public String getBoxValue() {

    return this.boxValue;
  }
}
