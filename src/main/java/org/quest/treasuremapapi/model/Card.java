package org.quest.treasuremapapi.model;

import lombok.ToString;
import org.quest.treasuremapapi.model.types.BoxTypes;


@ToString
public class Card extends BoxImpl {

  /**
   * @return the corresponding box type.
   */
  @Override
  public BoxTypes label() {

    return BoxTypes.CARD;
  }
}
