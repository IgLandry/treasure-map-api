package org.quest.treasuremapapi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.quest.treasuremapapi.model.types.BoxTypes;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Treasure extends BoxImpl {

  private int nbTreasures;

  /**
   * @return the corresponding box type.
   */
  @Override
  public BoxTypes label() {

    return BoxTypes.TREASURE;
  }
}
