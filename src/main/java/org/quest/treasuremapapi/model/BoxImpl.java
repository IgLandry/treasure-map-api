package org.quest.treasuremapapi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.quest.treasuremapapi.generictypes.Box;

@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class BoxImpl implements Box {

  private int vertical;
  private int horizontal;
  private String boxType;

  /**
   * @return the int value corresponding to index of vertical axe.
   */
  @Override
  public int getVertical() {

    return this.vertical;
  }

  /**
   * @return the int value corresponding to index of horizontal axe.
   */
  @Override
  public int getHorizontal() {

    return this.horizontal;
  }

  public String getBoxType() {

    return boxType;
  }

  /**
   * @param box the box to be compared.
   * @return negative integer, zero or positive integer as this box object is less than, equal to,
   * or greater than the specified box.
   */
  @Override
  public int compareTo(Box box) {

    return this
        .label()
        .compareTo(box.label());
  }
}
