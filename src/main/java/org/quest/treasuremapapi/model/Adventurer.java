package org.quest.treasuremapapi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.quest.treasuremapapi.model.types.BoxTypes;
import org.quest.treasuremapapi.model.types.OrientationTypes;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Adventurer extends BoxImpl {

  private String name;
  private OrientationTypes orientation;
  private String moveSequences;
  private int nbTreasures;

  /**
   * @return the first letter of the corresponding box type.
   */
  @Override
  public BoxTypes label() {

    return BoxTypes.ADVENTURER;
  }
}
