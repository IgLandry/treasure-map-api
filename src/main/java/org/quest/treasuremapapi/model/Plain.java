package org.quest.treasuremapapi.model;

import lombok.ToString;
import org.quest.treasuremapapi.model.types.BoxTypes;

@ToString
public class Plain extends BoxImpl {

  private static Plain instance;

  private Plain() {

  }

  public static Plain getInstance() {

    if (instance == null) {
      instance = new Plain();
    }
    return instance;
  }

  /**
   * @return the corresponding box type.
   */
  @Override
  public BoxTypes label() {

    return BoxTypes.PLAIN;
  }
}
