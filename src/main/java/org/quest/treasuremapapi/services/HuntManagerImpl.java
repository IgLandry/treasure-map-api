package org.quest.treasuremapapi.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.quest.treasuremapapi.generictypes.Box;
import org.quest.treasuremapapi.generictypes.BoxManager;
import org.quest.treasuremapapi.generictypes.FileManager;
import org.quest.treasuremapapi.generictypes.services.AdventurerManager;
import org.quest.treasuremapapi.generictypes.services.HuntManager;
import org.quest.treasuremapapi.generictypes.services.MapManager;
import org.quest.treasuremapapi.model.Adventurer;
import org.quest.treasuremapapi.model.Treasure;
import org.quest.treasuremapapi.model.types.BoxTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class HuntManagerImpl implements HuntManager {

  private final FileManager fileManager;
  private final MapManager mapManager;
  private final BoxManager boxManager;
  private final AdventurerManager adventurerManager;

  @Autowired
  public HuntManagerImpl(FileManager fileManager, MapManager mapManager, BoxManager boxManager,
                         AdventurerManager adventurerManager) {

    this.fileManager = fileManager;
    this.mapManager = mapManager;

    this.boxManager = boxManager;
    this.adventurerManager = adventurerManager;
  }


  /**
   * manage all the hunt.
   */
  @Override
  public List<List<List<Box>>> manageHunt() {

    var filesStrings = fileManager.readFile("src/main/resources/files/input/treasures-input.txt");
    var stringsToBoxes = boxManager.convertStringsToBoxes(filesStrings);

    List<Adventurer> adventurers = new ArrayList<>();
    var mapBoxes = mapManager.initMap(stringsToBoxes, adventurers);

    List<List<Box>> beforeManageBoxes = cloneWithoutReference(mapBoxes);

    adventurers.forEach(box -> beforeManageBoxes
        .get(box.getVertical())
        .set(box.getHorizontal(), box));

    adventurerManager.manageMove(mapBoxes, adventurers);

    List<List<Box>> pairedBoxes = cloneWithoutReference(mapBoxes);

    adventurers.forEach(box -> pairedBoxes
        .get(box.getVertical())
        .set(box.getHorizontal(), box));

    var boxes = new ArrayList<>(mapBoxes
        .stream()
        .flatMap(Collection::stream)
        .toList());
    boxes.set(0, stringsToBoxes.get(0));

    boxes.addAll(adventurers);
    List<String> boxesToStrings = boxManager.convertBoxesToStrings(boxes);

    fileManager.writeFile(boxesToStrings
            .stream()
            .filter(Predicate.not(String::isEmpty))
            .toList(),
        String.join("/", "src/main/resources/files/output", "treasures-output"));

    return List.of(beforeManageBoxes, pairedBoxes);
  }

  private List<List<Box>> cloneWithoutReference(List<List<Box>> boxes) {

    List<List<Box>> boxMap = new ArrayList<>();
    final var squares_number_width = boxes
        .get(0)
        .size();

    final var squares_number_height = boxes
        .size();

    for (int i = 0; i < squares_number_height; i++) {
      List<Box> boxList = new ArrayList<>();
      for (int j = 0; j < squares_number_width; j++) {
        var box = boxes
            .get(i)
            .get(j);
        if (box
            .label()
            .compareTo(BoxTypes.TREASURE) == 0) {
          Treasure treasure = (Treasure) box;
          var t = new Treasure();
          t.setHorizontal(treasure.getHorizontal());
          t.setVertical(treasure.getVertical());
          t.setNbTreasures(treasure.getNbTreasures());
          t.setBoxType(BoxTypes.TREASURE.name());
          boxList.add(t);
        } else {
          boxList.add(box);
        }
      }
      boxMap.add(boxList);
    }
    return boxMap;
  }

}
