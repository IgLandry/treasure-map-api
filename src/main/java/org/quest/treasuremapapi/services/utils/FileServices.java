package org.quest.treasuremapapi.services.utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.quest.treasuremapapi.generictypes.FileManager;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class FileServices implements FileManager {

  /**
   * @param fileName the name of the file to read.
   * @return the lines of the file in writing order without the comments.
   */
  @Override
  public List<String> readFile(String fileName) {

    try {
      return Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);

    } catch (IllegalArgumentException | FileSystemNotFoundException | IOException e) {
      log.error("Error reading file: " + e.getMessage());
    }
    return Collections.emptyList();
  }

  /**
   * @param strings containing lines to write to the output file.
   */
  @Override
  public String writeFile(List<String> strings, String fileName) {

    try {
      final var outputFile = String
          .join("_", fileName, String.valueOf(Date
              .from(Instant.now())
              .getTime()))
          .concat(".txt");
      final var path = Paths.get(outputFile);
      Files.createFile(path);
      Files.write(path, strings);

      return outputFile;
    } catch (IllegalArgumentException | FileSystemNotFoundException | IOException e) {
      log.error("Error writing file: " + e.getMessage());
    }

    return null;
  }
}
