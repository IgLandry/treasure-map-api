package org.quest.treasuremapapi.services.utils;

import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.quest.treasuremapapi.generictypes.Box;
import org.quest.treasuremapapi.generictypes.services.MapManager;
import org.quest.treasuremapapi.model.Adventurer;
import org.quest.treasuremapapi.model.Plain;
import org.quest.treasuremapapi.model.Treasure;
import org.quest.treasuremapapi.model.types.BoxTypes;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MapServices implements MapManager {

  private static final String FORMAT = "%-10s";

  /**
   * @param boxes to fill in the map at the beginning.
   * @return a 2-dimensional array referring to the map.
   */
  @Override
  public List<List<Box>> initMap(List<Box> boxes, List<Adventurer> adventurers) {

    List<List<Box>> boxMap = new ArrayList<>();
    final var squares_number_width = boxes
        .get(0)
        .getHorizontal();

    final var squares_number_height = boxes
        .get(0)
        .getVertical();

    for (int i = 0; i < squares_number_height; i++) {
      List<Box> boxList = new ArrayList<>();
      for (int j = 0; j < squares_number_width; j++) {
        boxList.add(Plain.getInstance());
      }
      boxMap.add(boxList);
    }

    boxes.forEach(box -> {
      if (box
          .label()
          .compareTo(BoxTypes.ADVENTURER) == 0) {
        adventurers.add((Adventurer) box);
      } else if (box
          .label()
          .compareTo(BoxTypes.MOUNTAIN) == 0) {
        boxMap
            .get(box.getVertical())
            .set(box.getHorizontal(), box);
      } else if (box
          .label()
          .compareTo(BoxTypes.TREASURE) == 0) {
        boxMap
            .get(box.getVertical())
            .set(box.getHorizontal(), box);
      }
    });
    return boxMap;
  }

  /**
   * @param boxesLists boxes to print like 2-dimension array.
   */
  @Override
  public void printMap(List<List<Box>> boxesLists) {

    boxesLists.forEach(boxes -> {
      var logLine = new StringBuilder();
      boxes.forEach(box -> {
        if (BoxTypes.PLAIN.compareTo(box.label()) == 0) {
          logLine.append(String.format(FORMAT, "."));
        }
        if (BoxTypes.MOUNTAIN.compareTo(box.label()) == 0) {
          logLine.append(String.format(FORMAT, box
              .label()
              .getBoxValue()));
        }
        if (BoxTypes.TREASURE.compareTo(box.label()) == 0) {
          logLine.append(String.format(FORMAT, box
              .label()
              .getBoxValue()
              .concat(String.join(String.valueOf(((Treasure) box).getNbTreasures()), "(", ")"))));
        }
        if (BoxTypes.ADVENTURER.compareTo(box.label()) == 0) {
          log.info(String.format(FORMAT, box
              .label()
              .getBoxValue()
              .concat(String.join(String.valueOf(((Adventurer) box).getName()), "(", ")"))));
        }
      });
      log.info(logLine.toString());
    });
  }
}
