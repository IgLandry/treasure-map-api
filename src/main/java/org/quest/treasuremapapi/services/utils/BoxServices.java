package org.quest.treasuremapapi.services.utils;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.quest.treasuremapapi.generictypes.Box;
import org.quest.treasuremapapi.generictypes.BoxManager;
import org.quest.treasuremapapi.model.Adventurer;
import org.quest.treasuremapapi.model.Card;
import org.quest.treasuremapapi.model.Mountain;
import org.quest.treasuremapapi.model.Treasure;
import org.quest.treasuremapapi.model.types.BoxTypes;
import org.quest.treasuremapapi.model.types.OrientationTypes;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BoxServices implements BoxManager {

  /**
   * @param boxes corresponding to all box after treasure hunt.
   * @return strings lines that were written to the output file.
   */
  @Override
  public List<String> convertBoxesToStrings(List<Box> boxes) {

    return boxes
        .stream()
        .sorted()
        .map(this::mapBoxToString)
        .toList();
  }

  /**
   * @param strings lines read from the input file.
   * @return all box for treasure hunt.
   */
  @Override
  public List<Box> convertStringsToBoxes(List<String> strings) {

    return strings
        .stream()
        .filter(s -> !s.startsWith("#"))
        .map(this::mapStringToBox)
        .toList();
  }

  private Box mapStringToBox(String string) {

    final var splitArray = string.split(" - ");

    if (string.startsWith(BoxTypes.CARD.getBoxValue())) {

      var card = new Card();
      card.setHorizontal(Integer.parseInt(splitArray[1]));
      card.setVertical(Integer.parseInt(splitArray[2]));
      return card;
    } else if (string.startsWith(BoxTypes.MOUNTAIN.getBoxValue())) {

      var mountain = new Mountain();
      mountain.setHorizontal(Integer.parseInt(splitArray[1]));
      mountain.setVertical(Integer.parseInt(splitArray[2]));
      mountain.setBoxType(BoxTypes.MOUNTAIN.name());
      return mountain;
    } else if (string.startsWith(BoxTypes.TREASURE.getBoxValue())) {

      var treasure = new Treasure();
      treasure.setHorizontal(Integer.parseInt(splitArray[1]));
      treasure.setVertical(Integer.parseInt(splitArray[2]));
      treasure.setNbTreasures(Integer.parseInt(splitArray[3]));
      treasure.setBoxType(BoxTypes.TREASURE.name());
      return treasure;
    } else if (string.startsWith(BoxTypes.ADVENTURER.getBoxValue())) {

      var adventurer = new Adventurer();
      adventurer.setHorizontal(Integer.parseInt(splitArray[2]));
      adventurer.setVertical(Integer.parseInt(splitArray[3]));
      adventurer.setName(splitArray[1]);
      adventurer.setBoxType(BoxTypes.ADVENTURER.name());
      if (OrientationTypes.SOUTH
          .getOrientationValue()
          .equals(splitArray[4])) {
        adventurer.setOrientation(OrientationTypes.SOUTH);
      } else if (OrientationTypes.NORTH
          .getOrientationValue()
          .equals(splitArray[4])) {
        adventurer.setOrientation(OrientationTypes.NORTH);
      } else if (OrientationTypes.EAST
          .getOrientationValue()
          .equals(splitArray[4])) {
        adventurer.setOrientation(OrientationTypes.EAST);
      } else if (OrientationTypes.WEST
          .getOrientationValue()
          .equals(splitArray[4])) {
        adventurer.setOrientation(OrientationTypes.WEST);
      }
      adventurer.setMoveSequences(splitArray[5]);
      return adventurer;
    } else {
      return null;
    }
  }

  private String mapBoxToString(Box box) {

    if (BoxTypes.ADVENTURER
        .compareTo(box.label()) == 0) {
      assert box instanceof Adventurer : "box must be an instance of Adventurer";
      final var adventurer = (Adventurer) box;
      return String.join(" - ", adventurer
              .label()
              .getBoxValue(), adventurer.getName(),
          String.valueOf(adventurer.getHorizontal()), String.valueOf(adventurer.getVertical()),
          adventurer
              .getOrientation()
              .getOrientationValue(), String.valueOf(adventurer.getNbTreasures()));
    } else if (box
        .label()
        .compareTo(BoxTypes.TREASURE) == 0) {
      assert box instanceof Treasure : "box must be an instance of Treasure";
      final var treasure = (Treasure) box;
      if (treasure.getNbTreasures() > 0) {
        return
            String.join(" - ", treasure
                    .label()
                    .getBoxValue(), String.valueOf(treasure.getHorizontal()),
                String.valueOf(treasure.getVertical()), String.valueOf(treasure.getNbTreasures()));
      }
    } else if (box
        .label()
        .compareTo(BoxTypes.PLAIN) != 0) {
      return
          String.join(" - ", box
                  .label()
                  .getBoxValue(), String.valueOf(box.getHorizontal()),
              String.valueOf(box.getVertical()));
    }
    return "";
  }
}
