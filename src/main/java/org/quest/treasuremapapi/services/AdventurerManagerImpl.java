package org.quest.treasuremapapi.services;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import lombok.extern.slf4j.Slf4j;
import org.quest.treasuremapapi.generictypes.Box;
import org.quest.treasuremapapi.generictypes.services.AdventurerManager;
import org.quest.treasuremapapi.generictypes.services.AdventurerService;
import org.quest.treasuremapapi.model.Adventurer;
import org.quest.treasuremapapi.model.types.MoveTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AdventurerManagerImpl implements AdventurerManager {

  private final AdventurerService adventurerService;

  @Autowired
  public AdventurerManagerImpl(AdventurerService adventurerService) {

    this.adventurerService = adventurerService;
  }

  /**
   * @param adventurers all adventurers who will be in the hunt.
   */
  @Override
  public void manageMove(List<List<Box>> boxesLists, List<Adventurer> adventurers) {

    AtomicBoolean checkMove = new AtomicBoolean(true);

    while (checkMove.get()) {
      checkMove.set(false);
      adventurers.forEach(adventurer -> {
        var sequence = adventurer.getMoveSequences();
        if (!sequence.isEmpty()) {
          var actionMove = sequence.split("")[0];
          var restOfSequence = sequence.substring(1);
          adventurer.setMoveSequences(restOfSequence);

          if (MoveTypes.RIGHT
              .getMoveValue()
              .equals(actionMove)) {
            adventurerService.turnToTheRight(adventurer);
          } else if (MoveTypes.LEFT
              .getMoveValue()
              .equals(actionMove)) {
            adventurerService.turnToTheLeft(adventurer);
          } else if (MoveTypes.MOVE
              .getMoveValue()
              .equals(actionMove)) {
            adventurerService.stepFroward(adventurer, boxesLists, adventurers);
          }
          checkMove.set(true);
        }
      });
    }
  }
}
