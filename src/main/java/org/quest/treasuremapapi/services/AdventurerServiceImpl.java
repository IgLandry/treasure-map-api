package org.quest.treasuremapapi.services;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.quest.treasuremapapi.exceptions.NullArgumentException;
import org.quest.treasuremapapi.generictypes.Box;
import org.quest.treasuremapapi.generictypes.services.AdventurerService;
import org.quest.treasuremapapi.model.Adventurer;
import org.quest.treasuremapapi.model.Treasure;
import org.quest.treasuremapapi.model.types.BoxTypes;
import org.quest.treasuremapapi.model.types.OrientationTypes;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AdventurerServiceImpl implements AdventurerService {

  private static final String ERROR_MESSAGE = "Adventurer provided is null.";

  /**
   * @param adventurer adventurer to take a step forward.
   */
  @Override
  public void stepFroward(Adventurer adventurer, List<List<Box>> boxesLists,
                          List<Adventurer> adventurers) {

    if (adventurer != null) {
      final var HEIGHT = boxesLists.size();
      final var WIDTH = boxesLists
          .get(0)
          .size();
      final var vertical = adventurer.getVertical();
      final var horizontal = adventurer.getHorizontal();
      if (adventurer
          .getOrientation()
          .compareTo(OrientationTypes.SOUTH) == 0 && vertical < HEIGHT - 1) {
        collectTreasures(adventurer, boxesLists, vertical + 1, horizontal, adventurers, true);
      } else if (adventurer
          .getOrientation()
          .compareTo(OrientationTypes.NORTH) == 0 && vertical > 0) {
        collectTreasures(adventurer, boxesLists, vertical - 1, horizontal, adventurers, true);
      } else if (adventurer
          .getOrientation()
          .compareTo(OrientationTypes.EAST) == 0 && horizontal < WIDTH - 1) {
        collectTreasures(adventurer, boxesLists, vertical, horizontal + 1, adventurers, false);
      } else if (adventurer
          .getOrientation()
          .compareTo(OrientationTypes.WEST) == 0 && (horizontal > 0)) {
        collectTreasures(adventurer, boxesLists, vertical, horizontal - 1, adventurers, false);
      }
    } else {
      throw new NullArgumentException(ERROR_MESSAGE);
    }
  }

  /**
   * @param adventurer adventurer to make a quarter turn to the right.
   */
  @Override
  public void turnToTheRight(Adventurer adventurer) {

    if (adventurer != null) {
      if (adventurer
          .getOrientation()
          .compareTo(OrientationTypes.SOUTH) == 0) {
        adventurer.setOrientation(OrientationTypes.WEST);
      } else if (adventurer
          .getOrientation()
          .compareTo(OrientationTypes.NORTH) == 0) {
        adventurer.setOrientation(OrientationTypes.EAST);
      } else if (adventurer
          .getOrientation()
          .compareTo(OrientationTypes.EAST) == 0) {
        adventurer.setOrientation(OrientationTypes.SOUTH);
      } else if (adventurer
          .getOrientation()
          .compareTo(OrientationTypes.WEST) == 0) {
        adventurer.setOrientation(OrientationTypes.NORTH);
      }
    } else {
      throw new NullArgumentException(ERROR_MESSAGE);
    }
  }

  /**
   * @param adventurer adventurer to make a quarter turn to the left.
   */
  @Override
  public void turnToTheLeft(Adventurer adventurer) {

    if (adventurer != null) {
      if (adventurer
          .getOrientation()
          .compareTo(OrientationTypes.SOUTH) == 0) {
        adventurer.setOrientation(OrientationTypes.EAST);
      } else if (adventurer
          .getOrientation()
          .compareTo(OrientationTypes.NORTH) == 0) {
        adventurer.setOrientation(OrientationTypes.WEST);
      } else if (adventurer
          .getOrientation()
          .compareTo(OrientationTypes.EAST) == 0) {
        adventurer.setOrientation(OrientationTypes.NORTH);
      } else if (adventurer
          .getOrientation()
          .compareTo(OrientationTypes.WEST) == 0) {
        adventurer.setOrientation(OrientationTypes.SOUTH);
      }
    } else {
      throw new NullArgumentException(ERROR_MESSAGE);
    }
  }

  private void collectTreasures(Adventurer adventurer, List<List<Box>> boxesLists,
                                int newVerticalVal, int newHorizontalVal,
                                List<Adventurer> adventurers, boolean vert) {

    final var adventurerLabel = boxesLists
        .get(newVerticalVal)
        .get(newHorizontalVal);

    boolean anotherAdventurerIsNotPresent = adventurers
        .stream()
        .filter(ad -> ad.getVertical() == newVerticalVal && ad.getHorizontal() == newHorizontalVal)
        .findAny()
        .isEmpty();

    if (adventurerLabel
        .label()
        .compareTo(BoxTypes.MOUNTAIN) != 0 && anotherAdventurerIsNotPresent) {
      if (adventurerLabel
          .label()
          .compareTo(BoxTypes.TREASURE) == 0) {
        final var treasure = ((Treasure) adventurerLabel);
        if (treasure.getNbTreasures() > 0) {
          adventurer.setNbTreasures(adventurer.getNbTreasures() + 1);
          treasure.setNbTreasures(treasure.getNbTreasures() - 1);
          boxesLists
              .get(newVerticalVal)
              .set(newHorizontalVal, treasure);
        }
      }
      if (vert) {
        adventurer.setVertical(newVerticalVal);
      } else {
        adventurer.setHorizontal(newHorizontalVal);
      }
    }
  }
}
