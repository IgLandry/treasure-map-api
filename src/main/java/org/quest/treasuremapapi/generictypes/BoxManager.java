package org.quest.treasuremapapi.generictypes;

import java.util.List;

public interface BoxManager {

  List<String> convertBoxesToStrings(List<Box> boxes);

  List<Box> convertStringsToBoxes(List<String> strings);
}
