package org.quest.treasuremapapi.generictypes;

import java.util.List;

public interface FileManager {

  List<String> readFile(String fileName);

  String writeFile(List<String> strings, String fileName);
}
