package org.quest.treasuremapapi.generictypes;

import org.quest.treasuremapapi.model.types.BoxTypes;

public interface Box extends Comparable<Box> {

  default BoxTypes label() {

    return BoxTypes.PLAIN;
  }

  int getVertical();

  int getHorizontal();
}
