package org.quest.treasuremapapi.generictypes.services;

import java.util.List;
import org.quest.treasuremapapi.generictypes.Box;
import org.quest.treasuremapapi.model.Adventurer;

public interface AdventurerManager {

  void manageMove(List<List<Box>> boxesLists, List<Adventurer> adventurers);
}
