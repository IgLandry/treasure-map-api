package org.quest.treasuremapapi.generictypes.services;

import java.util.List;
import org.quest.treasuremapapi.generictypes.Box;
import org.quest.treasuremapapi.model.Adventurer;

public interface AdventurerService {

  void stepFroward(Adventurer adventurer, List<List<Box>> boxesLists, List<Adventurer> adventurers);

  void turnToTheRight(Adventurer adventurer);

  void turnToTheLeft(Adventurer adventurer);
}
