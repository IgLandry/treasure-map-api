package org.quest.treasuremapapi.generictypes.services;

import java.util.List;
import org.quest.treasuremapapi.generictypes.Box;
import org.quest.treasuremapapi.model.Adventurer;

public interface MapManager {

  List<List<Box>> initMap(List<Box> boxes, List<Adventurer> adventurers);

  void printMap(List<List<Box>> boxesLists);
}
