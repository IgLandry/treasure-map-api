package org.quest.treasuremapapi.generictypes.services;

import java.util.List;
import org.quest.treasuremapapi.generictypes.Box;

public interface HuntManager {

  List<List<List<Box>>> manageHunt();
}
