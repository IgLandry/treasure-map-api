package org.quest.treasuremapapi.resources;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.quest.treasuremapapi.generictypes.Box;
import org.quest.treasuremapapi.generictypes.services.HuntManager;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/treasure/v1")
@RequiredArgsConstructor
@CrossOrigin("*")
public class HuntResources {

  private final HuntManager huntManager;

  @GetMapping(value = "/hunt")
  @Operation(summary = "Get boxes before and after hunt")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "success",
              content = {
                  @Content(array = @ArraySchema(schema = @Schema(implementation = Box.class)))
              }),
          @ApiResponse(responseCode = "400", description = "bad request", content = @Content),
          @ApiResponse(responseCode = "403", description = "forbidden", content = @Content)
      })
  public List<List<List<Box>>> launchHunt() {

    return huntManager.manageHunt();
  }
}
